<?php

namespace Koziar\Vendor\Api\Data;

interface VendorDatabaseInterface
{
    const DATABASE_KOZIAR_VENDOR_LIST                    = 'koziar_vendor_list';

    const DATABASE_KOZIAR_VENDOR_LIST_VENDOR_ID          = 'vendor_id';
    const DATABASE_KOZIAR_VENDOR_LIST_VENDOR_NAME        = 'vendor_name';
    const DATABASE_KOZIAR_VENDOR_LIST_VENDOR_DESCRIPTION = 'vendor_description';
    const DATABASE_KOZIAR_VENDOR_LIST_VENDOR_IMAGE       = 'vendor_image';
    const DATABASE_KOZIAR_VENDOR_LIST_TIME_OCCURRED      = 'time_occurred';

    /**
     * @return int
     */
    public function getVendorId();

    /**
     * @return string
     */
    public function getVendorName();

    /**
     * @return string
     */
    public function getVendorDescription();

    /**
     * @return string
     */
    public function getVendorImage();

    /**
     * @return string
     */
    public function getTimeOccurred();

    /**
     * @param $data
     * @return mixed
     */
    public function setVendorName($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setVendorDescription($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setVendorImage($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setTimeOccurred($data);

}
