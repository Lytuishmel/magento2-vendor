<?php

namespace Koziar\Vendor\Api;

use Koziar\Vendor\Api\Data\VendorDatabaseInterface;

interface VendorRepositoryInterface
{

    /**
     * save firstTest model
     * @param VendorDatabaseInterface $data
     *
     * @return mixed
     */
    public function save(VendorDatabaseInterface $data);

    /**
     * @param int $ItemId
     *
     * @return mixed
     */
    public function getById($ItemId);

    /**
     * @return mixed
     */
    public function getNewModel();
}
