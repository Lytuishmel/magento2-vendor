<?php
namespace Koziar\Vendor\Ui\Component\Source\Form;

use Koziar\Vendor\Model\ResourceModel\KoziarVendorList\CollectionFactory;
use Magento\Framework\UrlInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Store\Model\StoreManagerInterface;

class DataProvider extends AbstractDataProvider
{
    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData()
    {
        $result = [];
        $items  = $this->collection->getItems();

        foreach ($items as $item) {
            $data = $item->getData();
            if ($data['vendor_image']) {
                $img = [];
                $img[0]['image'] = $data['vendor_image'];
                $img[0]['url'] = $data['vendor_image'];
                $img[0]['url'] = sprintf('%s%s', $this->getBaseUrl(), $item['vendor_image']);
                $data['vendor_image'] = $img;
            }
            $result[$data['vendor_id']] = $data;
        }

        return $result;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBaseUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(
                UrlInterface::URL_TYPE_MEDIA
            );
    }

}
