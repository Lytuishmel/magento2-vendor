<?php

namespace Koziar\Vendor\Controller\Adminhtml\Vendorlist;

use Koziar\Vendor\Api\VendorRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;


class Delete extends Action
{
    private $pageFactory;
    protected $resultRedirectFactory;
    private $repository;

    public function __construct(
        RedirectFactory $redirectFactory,
        VendorRepositoryInterface $repository,
        PageFactory $pageFactory,
        Action\Context $context
    ) {
        $this->resultRedirectFactory = $redirectFactory;
        $this->repository = $repository;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Koziar_Vendor::vendor_list');
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('vendor_id');
        if ($id) {
            $model = $this->repository->getById($id);
            try {
                $model->delete();
                $this->messageManager->addSuccessMessage(__('Vendor Deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                return $resultRedirect->setPath('*/*/edit', ['vendor_id' => $id]);
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}
