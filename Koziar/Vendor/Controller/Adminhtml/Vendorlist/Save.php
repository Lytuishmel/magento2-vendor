<?php

namespace Koziar\Vendor\Controller\Adminhtml\Vendorlist;

use Koziar\Vendor\Api\VendorRepositoryInterface;
use Koziar\Vendor\Model\ImageUploader;
use Koziar\Vendor\Model\VendorDatabase;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{

    /**
     * @var VendorDatabase
     */
    private $model;

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var VendorRepositoryInterface
     */
    private $repository;

    /**
     * Image uploader
     *
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * Save constructor.
     * @param RedirectFactory $redirectFactory
     * @param VendorDatabase $vendorDatabase
     * @param VendorRepositoryInterface $repository
     * @param PageFactory $pageFactory
     * @param ImageUploader $imageUploader
     * @param Action\Context $context
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        VendorDatabase $vendorDatabase,
        VendorRepositoryInterface $repository,
        PageFactory $pageFactory,
        ImageUploader $imageUploader,
        Action\Context $context
    ) {
        $this->resultRedirectFactory = $redirectFactory;
        $this->model = $vendorDatabase;
        $this->repository = $repository;
        $this->pageFactory = $pageFactory;
        $this->imageUploader = $imageUploader;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Koziar_Vendor::vendor_list');
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            if (empty($data['vendor_id'])) {
                $data['vendor_id'] = null;
                $model = $this->repository->getNewModel();
            } else {
                $model = $this->repository->getById($data['vendor_id']);
            }

            if (isset($data['vendor_image'][0]['name']) && isset($data['vendor_image'][0]['tmp_name'])) {
                $baseImagePath = $this->imageUploader->moveFileFromTmp($data['vendor_image'][0]['name'], true);
            } else {
                $baseImagePath = '';
            }

            try {
                $data['vendor_image'] = $baseImagePath;
                $model->setData($data);
                $this->repository->save($model);
                $this->messageManager->addSuccessMessage(__('Vendor Saved Successfully'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['vendor_id' =>$model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
            return $resultRedirect->setPath('*/*/index');
        }

        return $resultRedirect->setPath('*/*/');
    }
}
