<?php
namespace Koziar\Vendor\Controller\Adminhtml\Vendorlist;

use Koziar\Vendor\Model\VendorDatabase;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{
    protected $vendorDatabaseModel;
    protected $pageFactory;
    protected $registry;

    public function __construct(
        PageFactory $pageFactory,
        VendorDatabase $vendorDatabaseModel,
        Registry $registry,
        Action\Context $context
    ) {
        $this->vendorDatabaseModel = $vendorDatabaseModel;
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Koziar_Vendor::vendor_list');
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('vendor_id');
        $model = $this->vendorDatabaseModel;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This vendor does not exists'));

                $result = $this->resultRedirectFactory->create();
                return $result->setPath("vendor/vendorlist/index");
            }
        }

        $data = $this->_getSession()->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        $this->registry->register('vendorVendorlist', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();

        $resultPage->addBreadcrumb(
            $id ? __('Edit Vendor') : __('Add a New Vendor'),
            $id ? __('Edit Vendor') : __('Add a New Vendor')
        );
        if ($id) {
            $resultPage->getConfig()->getTitle()->prepend('Edit');
        } else {
            $resultPage->getConfig()->getTitle()->prepend('Add');
        }
        return $this->pageFactory->create();
    }
}
