<?php

namespace Koziar\Vendor\Model\ResourceModel\KoziarVendorList;

use Koziar\Vendor\Model\VendorDatabase;
use Koziar\Vendor\Model\ResourceModel\KoziarVendorList as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = VendorDatabase::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_ID;

    protected function _construct()
    {
        $this->_init(VendorDatabase::class, ResourceModel::class);
    }
}
