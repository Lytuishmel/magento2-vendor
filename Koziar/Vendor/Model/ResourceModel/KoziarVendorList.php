<?php

namespace Koziar\Vendor\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Koziar\Vendor\Api\Data\VendorDatabaseInterface;

class KoziarVendorList extends AbstractDb
{

    protected function _construct()
    {
        $this->_init(VendorDatabaseInterface::DATABASE_KOZIAR_VENDOR_LIST, VendorDatabaseInterface::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_ID);
    }
}
