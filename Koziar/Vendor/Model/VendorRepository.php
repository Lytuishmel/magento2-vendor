<?php

namespace Koziar\Vendor\Model;

use Koziar\Vendor\Api\Data\VendorDatabaseInterface;
use Koziar\Vendor\Api\Data\VendorDatabaseInterfaceFactory as VendorDatabaseFactory;
use Koziar\Vendor\Api\VendorRepositoryInterface;
use Koziar\Vendor\Model\ResourceModel\KoziarVendorList as ResourceModel;
use Koziar\Vendor\Model\ResourceModel\KoziarVendorList\CollectionFactory;
use Magento\Framework\DB\Adapter\ConnectionException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;

class VendorRepository implements VendorRepositoryInterface
{
    private $resourceModel;

    private $collectionFactory;
    private $vendorDatabaseFactory;

    /**
     * Vendor Repository constructor.
     * @param ResourceModel $resourceModel
     * @param CollectionFactory $collectionFactory
     * @param VendorDatabaseFactory $vendorDatabaseFactory
     */
    public function __construct(
        ResourceModel $resourceModel,
        CollectionFactory $collectionFactory,
        VendorDatabaseFactory $vendorDatabaseFactory
    ) {
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->vendorDatabaseFactory = $vendorDatabaseFactory;
    }

    /**
     * @param VendorDatabaseInterface $data
     * @throws CouldNotSaveException
     * @throws \Magento\Framework\Exception\TemporaryState\CouldNotSaveException
     */
    public function save(VendorDatabaseInterface $data)
    {
        if ($data) {
            try {
                $this->resourceModel->save($data);
            } catch (ConnectionException $exception) {
                throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                    __('Database connection error'),
                    $exception,
                    $exception->getCode()
                );
            } catch (CouldNotSaveException $e) {
                throw new CouldNotSaveException(__('Unable to save product'), $e);
            } catch (ValidatorException $e) {
                throw new CouldNotSaveException(__($e->getMessage()));
            } catch (AlreadyExistsException $e) {
                throw new CouldNotSaveException(__('Already exist'));
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Something went wrong'));
            }
        }
    }

    /**
     * @param int $ItemId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($ItemId)
    {
        $model = $this->vendorDatabaseFactory->create();
        $this->resourceModel->load($model, $ItemId);

        if (!$model->getId()) {
            throw new NoSuchEntityException(__('Requested tweet doesn\'t exist'));
        }
        return $model;
    }

    /**
     * @return VendorDatabaseInterface
     */
    public function getNewModel()
    {
        return $this->vendorDatabaseFactory->create();
    }
}
