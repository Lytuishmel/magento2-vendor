<?php

namespace Koziar\Vendor\Model;

use Koziar\Vendor\Api\Data\VendorDatabaseInterface;
use Magento\Framework\Model\AbstractModel;
use Koziar\Vendor\Model\ResourceModel\KoziarVendorList as ResourceModel;

class VendorDatabase extends AbstractModel implements VendorDatabaseInterface
{

    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @return int
     */
    public function getVendorId()
    {
        return $this->getData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_ID);
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
        return $this->getData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_NAME);
    }

    /**
     * @return string
     */
    public function getVendorDescription()
    {
        return $this->getData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_DESCRIPTION);
    }

    /**
     * @return string
     */
    public function getVendorImage()
    {
        return $this->getData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_IMAGE);
    }

    /**
     * @return string
     */
    public function getTimeOccurred()
    {
        return $this->getData(self::DATABASE_KOZIAR_VENDOR_LIST_TIME_OCCURRED);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setVendorName($data)
    {
        return $this->setData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_NAME, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setVendorDescription($data)
    {
        return $this->setData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_DESCRIPTION, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setVendorImage($data)
    {
        return $this->setData(self::DATABASE_KOZIAR_VENDOR_LIST_VENDOR_IMAGE, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setTimeOccurred($data)
    {
        return $this->setData(self::DATABASE_KOZIAR_VENDOR_LIST_TIME_OCCURRED, $data);
    }
}
