<?php

namespace Koziar\Vendor\Model\Attribute\Source;

use Koziar\Vendor\Model\ResourceModel\KoziarVendorList\CollectionFactory;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Vendor extends AbstractSource
{

    protected $collectionFactory;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function getAllOptions()
    {
        if (!$this->_options) {
            /** @var Collection $collection */
            $collection = $this->collectionFactory->create();

            $vendors = $collection->getItems();

            if (!$vendors) {
                return [];
            }

            $options = [];

            foreach ($vendors as $vendor) {
                $options[] = [
                    'label' => $vendor->getData('vendor_name'),
                    'value' => $vendor->getData('vendor_name')
                ];
            }

            $this->_options = $options;
        }

        return $this->_options;
    }

}
