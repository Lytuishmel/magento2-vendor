<?php

namespace Koziar\Vendor\Block\Adminhtml\Vendorlist\Edit;

use Koziar\Vendor\Model\VendorDatabase;

class GenericButton
{
    protected $urlBuilder;

    protected $registry;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }

    public function getId()
    {
        /** @var VendorDatabase $vendorList */
        $vendorList = $this->registry->registry('vendorVendorlist');

        return $vendorList ? $vendorList->getId() : null;
    }

    public function getUrl($route='', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
