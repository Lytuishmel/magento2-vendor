<?php

namespace Koziar\Vendor\Block;

use Koziar\Vendor\Model\ResourceModel\KoziarVendorList\CollectionFactory;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;

class VendorLogo extends Template
{
    /**
     * @var CollectionFactory
     */
    private $collection;

    /**
     * @var AbstractProduct
     */
    private $abstractProduct;

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * VendorProductInfo constructor.
     * @param Template\Context $context
     * @param CollectionFactory $collection
     * @param AbstractProduct $abstractProduct
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CollectionFactory $collection,
        AbstractProduct $abstractProduct,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->collection = $collection;
        $this->abstractProduct = $abstractProduct;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getVendorLogo()
    {
        $attributeValue = $this->abstractProduct->getProduct()->getCustomAttribute('product_vendor');

        if (isset($attributeValue)) {
            $vendors = explode(',', $attributeValue->getValue());

            $collection = $this->collection->create();
            $collection->addFieldToSelect('vendor_image')
                ->addFieldToFilter('vendor_name', ['in' => $vendors]);
            $items = $collection->getData();
            foreach ($items as $key => &$item) {
                if (empty($item['vendor_image'])) {
                    unset($items[$key]);
                } else {
                    $item['vendor_image'] = sprintf('%s%s', $this->getBaseUrl(), $item['vendor_image']);
                }
            }
            return $items;
        } else {
            return [];
        }
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBaseUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(
                UrlInterface::URL_TYPE_MEDIA
            );
    }
}
