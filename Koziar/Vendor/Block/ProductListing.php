<?php

namespace Koziar\Vendor\Block;

use Koziar\Vendor\Model\ResourceModel\KoziarVendorList\CollectionFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;

class ProductListing extends ListProduct
{
    /**
     * @var CollectionFactory
     */
    private $collection;

    /**
     * VendorProductInfo constructor.
     * @param Context $context
     * @param CollectionFactory $collection
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $collection,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        array $data = []
    ) {
        $this->collection = $collection;
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
    }

    /**
     * @param $product
     *
     * @return array
     */
    public function getVendorData($product)
    {
        $attributeValue = $product->getCustomAttribute('product_vendor');

        if (isset($attributeValue)) {
            $vendors = explode(',', $attributeValue->getValue());

            $collection = $this->collection->create();
            $collection->addFieldToSelect('vendor_name')
                ->addFieldToSelect('vendor_description')
                ->addFieldToFilter('vendor_name', ['in' => $vendors]);
            return $collection->getData();
        } else {
            return [];
        }
    }
}
